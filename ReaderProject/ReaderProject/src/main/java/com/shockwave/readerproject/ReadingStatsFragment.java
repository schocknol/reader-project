package com.shockwave.readerproject;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.CustomLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import org.androidannotations.annotations.EFragment;

import java.util.Date;

/**
 * Fragment containing graph of all the users reading stats
 */
@EFragment
public class ReadingStatsFragment extends Fragment {
    private GraphView graphView;
    private LinearLayout linearLayout;
    private TextView textView;
    Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reading_stats, container, false);
        linearLayout = (LinearLayout) v.findViewById(R.id.layoutStats);

        ReaderDatabaseHandler db = new ReaderDatabaseHandler(mContext);
        GraphViewSeries stats = db.getAllStatsGraph();
        db.close();

        if (stats == null) {
            textView = new TextView(mContext);
            textView.setText("No stats yet. Go take the Speed Test!");
            linearLayout.addView(textView);
            return v;
        }

        final java.text.DateFormat dateTimeFormatter = DateFormat.getMediumDateFormat(mContext);
        graphView = new LineGraphView(getActivity(), "Reading Speed");
        graphView.setCustomLabelFormatter(new CustomLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return dateTimeFormatter.format(new Date((long) value));
                }
                return null;
            }
        });
        graphView.addSeries(stats);
        linearLayout.addView(graphView);
        return v;
    }

    public void resetStats() {
        ReaderDatabaseHandler handler = new ReaderDatabaseHandler(mContext);
        handler.deleteAllStats();
        handler.close();
        if (graphView != null && linearLayout != null) {
            graphView.removeAllSeries();
            linearLayout.removeAllViews();
            if (textView == null) {
                textView = new TextView(mContext);
                textView.setText("No stats yet. Go take the Speed Test!");
            }
            linearLayout.addView(textView);
        }
    }
}

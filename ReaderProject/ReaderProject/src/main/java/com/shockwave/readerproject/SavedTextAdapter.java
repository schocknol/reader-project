package com.shockwave.readerproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for SavedText List, allowing operations on SavedText objects
 * Created by Nolan Schock, 2014
 */
public class SavedTextAdapter extends ArrayAdapter<SavedTextHolder.SavedText> {
    private final Context context;
    private final List<SavedTextHolder.SavedText> objects;
    private List<SavedTextHolder.SavedText> selected = new ArrayList<SavedTextHolder.SavedText>();

    public SavedTextAdapter(Context context, int resource, List<SavedTextHolder.SavedText>
            objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.my_two_line_list_item, parent, false);

        TextView title = (TextView) rowView.findViewById(R.id.tvTitleItem);
        TextView author = (TextView) rowView.findViewById(R.id.tvAuthorItem);

        title.setText(objects.get(position).title);
        author.setText(objects.get(position).author);

        return rowView;
    }

    @Override
    public SavedTextHolder.SavedText getItem(int position) {
        return objects.get(position);
    }

    public SavedTextHolder.SavedText getSelection(int position) {
        return selected.get(position);
    }

    public void addSelection(int position) {
        selected.add(getItem(position));
    }

    public void removeSelectedItem(int position) {
        //getItem allowable because list cannot have duplicates
        selected.remove(getItem(position));
    }

    public void removeSelected() {
        objects.removeAll(selected);
    }

    public List<SavedTextHolder.SavedText> getSelected() {
        return selected;
    }

    public void resetSelected() {
        selected.clear();
        notifyDataSetChanged();
    }

    public int getSelectionSize() {
        return selected.size();
    }

    public void resetAll() {
        //Remove then add instead of retain just in case one of the default items has been deleted
        objects.removeAll(objects);
        objects.addAll(SavedTextHolder.DEFAULT_ITEMS);
    }
}

package com.shockwave.readerproject.listsections;

/**
 * Interface provides mechanism for supplying titles for instances based on the property they are
 * compared against. The parameterized type of the <b>Sectionizer</b> should be same as that of the
 * {@link SimpleSectionAdapter}.
 *
 * @author Ragunath Jawahar R <rj@mobsandgeeks.com>
 * @version 1.0
 */
public interface Sectionizer<T> {

    /**
     * Returns the title for the given instance from the data source.
     *
     * @param instance The instance obtained from the data source of the decorated list adapter.
     * @return section title for the given instance.
     */
    String getSectionTitleForItem(T instance);
}

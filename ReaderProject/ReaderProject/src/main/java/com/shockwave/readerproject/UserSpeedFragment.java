package com.shockwave.readerproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;

/**
 * Dialog fragment displaying the user's current speed from Speed Test
 * Created by Nolan Schock, 2014
 */
@EFragment
public class UserSpeedFragment extends DialogFragment {
    private UserSpeedListener mListener;

    static UserSpeedFragment newInstance(int speed, ArrayList<SavedTextHolder.SavedText> texts) {
        UserSpeedFragment fragment = new UserSpeedFragment_();

        Bundle args = new Bundle();
        args.putInt("speed", speed);
        args.putParcelableArrayList("texts", texts);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            mListener = (UserSpeedListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment().toString() + " must implement " +
                                         "UserSpeedListener");
        } catch (NullPointerException e) {
            throw new NullPointerException("Must set target fragment.");
        }

        final int speed = getArguments().getInt("speed");
        final ArrayList<SavedTextHolder.SavedText> texts = getArguments().getParcelableArrayList
                ("texts");
        final SpeedTestFragment testFragment = (SpeedTestFragment) getTargetFragment();
        final int testNum = testFragment.getTestNum();

        String message = createMessage(speed, testNum, texts.size());
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Your Speed");
        if (testNum > 0 && testNum < texts.size()) {
            //Keep Testing
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onUserSpeedCancelClick();
                }
            });
            builder.setNeutralButton("Stop", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onUserSpeedStopClick();
                }
            });

            builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onUserSpeedContinueClick();
                }
            });
        } else {
            //Done Testing
            ReaderDatabaseHandler handler = new ReaderDatabaseHandler(getActivity());
            handler.addStat(System.currentTimeMillis(), speed);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onUserSpeedOKClick();

                }
            });

            builder.setNeutralButton("Stats", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onUserSpeedStatsClick();
                }
            });
        }
        builder.setMessage(message);
        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mListener.onUserSpeedCancelClick();
    }

    private String createMessage(int speed, int testNum, int size) {
        String message = "Your speed after " + testNum + " test passage(s) is " + speed + " words" +
                         " per minute.";
        if (testNum >= size || testNum < 0) {
            message = "Your final speed is " + speed + " words per minute.";
            if (speed < 200) {
                message += "\n\nThis is below average for standard reading. Keep practicing!";
            } else if (speed < 325) {
                message += "\n\nThis is the average reading speed for most adults. Try to beat it!";
            } else if (speed < 500) {
                message += "\n\nThis is above average. Good job! Try to beat it!";
            } else {
                message += "\n\nWow, you read much faster than average! Congratulations on being a Speed Reader! Try to read even faster!";
            }
        } else {
            message += "\n\nContinue Testing?";
        }
        return message;
    }

    /**
     * Listener implemented in Speed Test to determine what to do on each button click
     */
    public interface UserSpeedListener {
        public void onUserSpeedCancelClick();

        public void onUserSpeedStopClick();

        public void onUserSpeedContinueClick();

        public void onUserSpeedOKClick();

        public void onUserSpeedStatsClick();
    }
}

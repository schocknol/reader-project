package com.shockwave.readerproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import org.androidannotations.annotations.EFragment;

/**
 * Dialog Fragment to Change Text Information -- for editing saved texts, adding saved texts, changing current text
 * Created by Nolan Schock, 2014
 */
@EFragment
public class ChangeTextInfoFragment extends DialogFragment {
    private static final int TEXT_LIMIT = 4000;

    private ChangeTextInfoListener mListener;
    private SavedTextHolder.SavedText mText;


    static ChangeTextInfoFragment newInstance(SavedTextHolder.SavedText text) {
        ChangeTextInfoFragment fragment = new ChangeTextInfoFragment_();

        Bundle args = new Bundle();
        args.putParcelable("text", text);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            mText = getArguments().getParcelable("text");
        } catch (NullPointerException e) {
            mText = new SavedTextHolder.SavedText();
        }


        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.fragment_change_text_info, null);

        final EditText etTitle = (EditText) view.findViewById(R.id.etTitle);
        etTitle.setText(mText.title);
        etTitle.requestFocus();

        final EditText etAuthor = (EditText) view.findViewById(R.id.etAuthor);
        etAuthor.setText(mText.author);

        //if custom savedtext (mText.timeCreated == -1), go next to content.
        // else, done after author (user can still edit content if they wish)
        if (mText.timeCreated == -1 && mText.content.length() < TEXT_LIMIT) {
            etAuthor.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        } else {
            etAuthor.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }

        final EditText etContent = (EditText) view.findViewById(R.id.etContent);
        //do not display text to edit if past 4000 characters for performance issues
        if (mText.content.length() > TEXT_LIMIT) {
            etContent.setText("Text too large to edit.");
            etContent.setEnabled(false);
        } else {
            etContent.setText(mText.content);
            //expand when focused, otherwise collapse
            etContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    etContent.setSingleLine(!hasFocus);
                }
            });
        }

        try {
            mListener = (ChangeTextInfoListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment().toString() + " must implement " +
                                         "ChangeTextInfoListener");
        } catch (NullPointerException e) {
            throw new NullPointerException("Must set target fragment.");
        }

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onChangeTextPositiveClick(new SavedTextHolder.SavedText(
                                etTitle.getText().toString(),
                                etAuthor.getText().toString(),
                                mText.content.length() < TEXT_LIMIT ? etContent.getText().toString() :
                                mText.content,
                                mText.timeCreated == -1 ? SystemClock.elapsedRealtime() : mText.timeCreated
                        ), mText);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }

    /**
     * Listener implemented in target fragment (Reader or Saved Text) to set text in fragment as applicable
     */
    public interface ChangeTextInfoListener {
        public void onChangeTextPositiveClick(SavedTextHolder.SavedText newText, SavedTextHolder
                .SavedText oldText);
    }
}

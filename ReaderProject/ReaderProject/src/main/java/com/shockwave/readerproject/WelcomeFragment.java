package com.shockwave.readerproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import org.androidannotations.annotations.EFragment;

@EFragment
public class WelcomeFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Welcome")
                .setMessage(R.string.welcome_text)
                .setNeutralButton("About",
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which) {
                                          ((MainActivity) getActivity()).replaceFragment(new AboutFragment_());
                                      }
                                  }
                                 )
                .setPositiveButton("OK",
                                   new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                       }
                                   }
                                  )
                .create();
    }
}

package com.shockwave.readerproject;

import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Display Options Dialog Fragment showing user options for changing theme/text size
 * Created by Nolan Schock, 2014
 */
@EFragment(R.layout.fragment_display_options)
public class DisplayOptionsFragment extends DialogFragment {

    @ViewById
    TextView tvFontSize;

    @ViewById
    Spinner spinnerTheme;

    @Pref
    ReaderPrefs_ prefs;

    @Click(R.id.bIncreaseFont)
    void increaseFontSize() {
        prefs.fontSizeGeneral().put(prefs.fontSizeGeneral().get() + 1);
        prefs.fontSizeFlow().put(prefs.fontSizeFlow().get() + 2);
        tvFontSize.setText((prefs.fontSizeGeneral().get() * 100) / 16 + "%");
        getActivity().recreate();
    }

    @Click(R.id.bDecreaseFont)
    void decreaseFontSize() {
        prefs.fontSizeGeneral().put(prefs.fontSizeGeneral().get() - 1);
        prefs.fontSizeFlow().put(prefs.fontSizeFlow().get() - 2);
        tvFontSize.setText((prefs.fontSizeGeneral().get() * 100) / 16 + "%");
        getActivity().recreate();
    }

    @Click(R.id.bDisplayOK)
    void closeDialog() {
        this.dismiss();
    }

    @AfterViews
    void setFontSizeText() {
        tvFontSize.setText((prefs.fontSizeGeneral().get() * 100) / 16 + "%");
    }

    @AfterViews
    void setupSpinner() {
        ArrayAdapter<CharSequence> themeAdapter =
                ArrayAdapter.createFromResource(getActivity(), R.array.theme_array,
                                                android.R.layout.simple_spinner_item);
        themeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTheme.setAdapter(themeAdapter);

        if (prefs.mThemeId().get() == R.style.AppTheme_Light) {
            spinnerTheme.setSelection(0, false);
        } else {
            spinnerTheme.setSelection(1, false);
        }
        spinnerTheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int oldThemeId = prefs.mThemeId().get();
                switch (position) {
                    case 0:
                        prefs.mThemeId().put(R.style.AppTheme_Light);
                        if (oldThemeId != R.style.AppTheme_Light) {
                            getActivity().recreate();
                        }
                        break;
                    case 1:
                        prefs.mThemeId().put(R.style.AppTheme_Dark);
                        if (oldThemeId != R.style.AppTheme_Dark) {
                            getActivity().recreate();
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @AfterViews
    void setTitle() {
        getDialog().setTitle("Display Options");
    }
}

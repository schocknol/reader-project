package com.shockwave.readerproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.doomonafireball.betterpickers.numberpicker.NumberPickerBuilder;
import com.doomonafireball.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.ipaulpro.afilechooser.utils.FileUtils;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Abstract Fragment which all Reader Fragments (Chunk Reader, Word Flow, Speed Test) inherit
 * Supplies methods/fields generally used between all reader fragments (at the very least, shared
 * between Chunk Reader and Word Flow)
 */
@EFragment
public abstract class ReaderFragment extends Fragment implements NumberPickerDialogFragment
                                                                         .NumberPickerDialogHandler,
                                                                 SavedTextFragment
                                                                         .OnSavedTextSelectedListener,
                                                                 PopupMenu.OnMenuItemClickListener,
                                                                 ChangeTextInfoFragment
                                                                         .ChangeTextInfoListener {
    public static final int REQUEST_OPEN_DOC = 12;
    public static final int REQUEST_READER_FRAG = 39;

    public static final int WPC_REFERENCE = 42;
    public static final int SPEED_REFERENCE = 43;

    public static final int CURRENT_TEXT_KEY_TEST = -69;
    public static final int CURRENT_TEXT_KEY_CHUNK = -70;
    public static final int CURRENT_TEXT_KEY_FLOW = -71;

    Context mContext;
    ReaderDatabaseHandler dbHandler;

    @InstanceState
    protected boolean isRunning = false;

    @InstanceState
    boolean pauseClick = false;

    @Pref
    ReaderPrefs_ prefs;

    @OptionsMenuItem(R.id.action_change_speed)
    MenuItem menuSpeed;

    @OptionsMenuItem(R.id.action_words_chunk)
    MenuItem menuWpc;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuWpc.setTitle("Words Per Chunk (" + prefs.wpc().get() + ")");
        menuSpeed.setTitle("Change Speed (" + prefs.wpm().get() + ")");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        dbHandler = new ReaderDatabaseHandler(mContext);
    }

    @OptionsItem(R.id.action_words_chunk)
    void changeWordsPerChunk() {
        isRunning = false;
        NumberPickerBuilder npb = new NumberPickerBuilder()
                .setReference(WPC_REFERENCE)
                .setFragmentManager(getChildFragmentManager())
                .setStyleResId(prefs.mThemeId().get() == R.style.AppTheme_Light ? R.style
                        .BetterPickersDialogFragment_Light : R.style.BetterPickersDialogFragment)
                .setTargetFragment(this)
                .setLabelText("Change Chunk Size " + limitOrUnlimited(prefs.chunkLimit().get()))
                .setDecimalVisibility(View.GONE)
                .setMinNumber(1)
                .setPlusMinusVisibility(View.GONE);
        npb.show();
    }

    @OptionsItem(R.id.action_change_speed)
    void changeReaderSpeed() {
        isRunning = false;
        NumberPickerBuilder npb = new NumberPickerBuilder()
                .setReference(SPEED_REFERENCE)
                .setFragmentManager(getChildFragmentManager())
                .setStyleResId(prefs.mThemeId().get() == R.style.AppTheme_Light ? R.style
                        .BetterPickersDialogFragment_Light : R.style.BetterPickersDialogFragment)
                .setTargetFragment(this)
                .setLabelText("Change Speed " + limitOrUnlimited(prefs.speedLimit().get()))
                .setDecimalVisibility(View.GONE)
                .setMinNumber(1)
                .setPlusMinusVisibility(View.GONE);
        npb.show();
    }

    private String limitOrUnlimited(int limit) {
        if (prefs.isPremium().get()) {
            return "";
        } else {
            return "(Max: " + limit + ")";
        }
    }

    @OptionsItem(R.id.action_save_text)
    void saveText() {
        SavedTextHolder.SavedText curText = getText();
        if (curText.title.equals("Unknown") || curText.author.equals("Unknown")) {
            //Text Unknown, prompt user to enter info they would like to save it under
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ChangeTextInfoFragment fragment = ChangeTextInfoFragment_.newInstance(curText);
            fragment.setTargetFragment(this, REQUEST_READER_FRAG);
            fragment.show(ft, "dialog");
        } else {
            //Text known, save if not already saved
            if (prefs.isPremium().get() || dbHandler.getAllTexts().size() < prefs.saveLimit().get()) {
                try {
                    dbHandler.addText(curText);
                    Toast.makeText(mContext, "Text saved.", Toast.LENGTH_SHORT).show();
                } catch (SQLiteConstraintException e) {
                    Toast.makeText(mContext, "Text already saved.", Toast.LENGTH_SHORT).show();
                }
            } else {
                MainActivity activity = (MainActivity) getActivity();
                activity.showPurchaseDialog(MainActivity.SKU_SAVE);
            }
        }
    }

    @OptionsItem(R.id.action_change_text)
    void selectText() {
        showTextsPopup(getActivity().findViewById(R.id.action_change_text));
    }

    /**
     * Show popup menu displaying locations from which to select text
     */
    public void showTextsPopup(View v) {
        PopupMenu popupMenu = new PopupMenu(mContext, v);
        popupMenu.setOnMenuItemClickListener(this);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.text_locs, popupMenu.getMenu());
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.loc_saved:
                MainActivity activity = ((MainActivity) getActivity());
                SavedTextFragment fragment = new SavedTextFragment_();
                fragment.setTargetFragment(this, REQUEST_READER_FRAG);
                activity.replaceFragment(fragment);
                return true;
            case R.id.loc_device:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/plain");
                    startActivityForResult(intent, REQUEST_OPEN_DOC);
                } else {
                    Intent intent = FileUtils.createGetContentIntent();
                    startActivityForResult(Intent.createChooser(intent, "Select a file."),
                                           REQUEST_OPEN_DOC);
                }
                return true;
            case R.id.loc_enter:
                ChangeTextInfoFragment changeTextInfoFragment = ChangeTextInfoFragment_
                        .newInstance(new SavedTextHolder.SavedText("Unknown", "Unknown", "", -1));
                changeTextInfoFragment.setTargetFragment(this, REQUEST_READER_FRAG);
                changeTextInfoFragment.show(getFragmentManager().beginTransaction(), "dialog");
                return true;
            default:
                return false;
        }
    }


    @Override
    public void onChangeTextPositiveClick(SavedTextHolder.SavedText newText, SavedTextHolder
            .SavedText oldText) {
        setTextFromSavedText(newText);
        if (oldText.timeCreated != -1) {
            //saving unknown/new text to saved texts
            try {
                if (prefs.isPremium().get() || dbHandler.getAllTexts().size() < prefs.saveLimit().get()) {
                    dbHandler.replaceText(newText);
                    if (getActivity().findViewById(R.id.list_fragment_layout) != null) {
                        //split pane
                        SavedTextFragment listFrag = (SavedTextFragment) getActivity()
                                .getSupportFragmentManager().findFragmentById(R.id.list_fragment_layout);
                        if (listFrag != null) {
                            listFrag.mAdapter.add(newText);
                            listFrag.mAdapter.notifyDataSetChanged();
                            listFrag.mListView.invalidateViews();
                        }
                    }
                    Toast.makeText(mContext, "Text saved.", Toast.LENGTH_SHORT).show();
                } else {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.showPurchaseDialog(MainActivity.SKU_SAVE);
                }
            } catch (SQLiteConstraintException e) {
                Toast.makeText(mContext, "Text already saved.",
                               Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSavedTextSelected(SavedTextHolder.SavedText text) {
        setTextFromSavedText(text);
        if (getActivity().findViewById(R.id.list_fragment_layout) == null) {
            ((MainActivity) getActivity()).replaceFragment(this);
        }
    }

    protected void startReading(Handler handler, Runnable runnable, int wordsPerMin) {
        if (!isAdded()) {
            return;
        }

        ((MainActivity) getActivity()).hideSystemUI();
        pauseClick = false;

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000 / getWordsPerSec(wordsPerMin));

        isRunning = true;
    }

    protected void stopReading(Handler handler, Runnable runnable) {
        if (!isAdded()) {
            return;
        }

        handler.removeCallbacks(runnable);
        isRunning = false;
        ((MainActivity) getActivity()).showSystemUI();
    }

    public abstract void resetReader();

    private void parseTextFile(Uri uri) {
        try {
            String text = readTextFromUri(uri);
            setTextFromString(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        inputStream.close();
        return stringBuilder.toString();
    }

    protected abstract void setTextFromString(String text);

    protected abstract void setTextFromSavedText(SavedTextHolder.SavedText text);

    protected abstract SavedTextHolder.SavedText getText();

    protected void setWordsPerChunk(int wpc) {
        if (prefs.isPremium().get() || wpc <= prefs.chunkLimit().get()) {
            prefs.wpc().put(wpc);
            menuWpc.setTitle("Words Per Chunk (" + prefs.wpc().get() + ")");
        } else {
            ((MainActivity) getActivity()).showPurchaseDialog(MainActivity.SKU_CHUNK);
        }
    }

    protected void setWordsPerMin(int wordsPerMin) {
        if (prefs.isPremium().get() || wordsPerMin <= prefs.speedLimit().get()) {
            prefs.wpm().put(wordsPerMin);
            menuSpeed.setTitle("Change Speed (" + prefs.wpm().get() + ")");
        } else {
            ((MainActivity) getActivity()).showPurchaseDialog(MainActivity.SKU_SPEED);
        }
    }

    protected int getWordsPerSec(int wordsPerMin) {
        int wps = wordsPerMin / 60;
        if (wps == 0) {
            return 1;
        }
        return wps;
    }

    @OnActivityResult(REQUEST_OPEN_DOC)
    void onFileResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            final Uri uri = data.getData();
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);

            int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            cursor.moveToFirst();
            final String extension = FilenameUtils.getExtension(uri.getPath());
            //File too big to properly read in Chunk Reader and Speed Test for performance issues
            //(In Chunk Reader, textview text set every time chunk moves forward. This is VERY bad for performance
            if (cursor.getLong(sizeIndex) > 350000) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Warning")
                        .setMessage("The file you are about to read is very large. This may " +
                                    "impact performance, as Reader Project is meant for smaller " +
                                    "texts." +
                                    " Are you sure you want to continue?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (extension.equals("txt")) {
                                    parseTextFile(uri);
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            } else {
                if (extension.equals("txt")) {
                    parseTextFile(uri);
                }
            }
        }
    }

    @UiThread(delay = 500)
    void hidePauseButton(FrameLayout layout) {
        layout.setForeground(null);
    }

    @UiThread(delay = 3000)
    void hideUIOnDelay(MainActivity activity) {
        if (isRunning) {
            activity.hideSystemUI();
        }
    }
}

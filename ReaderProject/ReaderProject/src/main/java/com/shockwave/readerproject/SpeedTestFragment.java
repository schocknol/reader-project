package com.shockwave.readerproject;

import android.os.SystemClock;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Speed Test Fragment displaying a text passage and allowing the user to read it.
 * Records user speed over 3 passages and gets average speed, which is used to gauge the user's
 * typical reading speed
 */
@EFragment(R.layout.fragment_speed_test)
public class SpeedTestFragment extends ReaderFragment implements UserSpeedFragment.UserSpeedListener {
    private int totalSpeed = 0;
    private int averageSpeed = 0;
    private double passageSpeed = 0;

    @InstanceState
    double startTime;

    @InstanceState
    SavedTextHolder.SavedText currentText;

    @InstanceState
    int testNumber = 1;

    @ViewById
    ScrollView scrollViewTester;

    @ViewById
    TextView tvSpeedTest;

    @ViewById
    FrameLayout testLayout;

    public SpeedTestFragment() {
        testNumber = 1;
    }

    @AfterViews
    void setInitialText() {
        tvSpeedTest.setTextSize(prefs.fontSizeGeneral().get());
        if (currentText == null) {
            currentText = dbHandler.getCurrentText(CURRENT_TEXT_KEY_TEST);
        }
        tvSpeedTest.setText(currentText.content);
        resetSpeeds();
        if (isRunning) {
            hideUiForTesting();
        }
    }

    @Override
    void selectText() {
        if (isRunning) {
            stopTesting();
        }
        super.selectText();
    }

    @Override
    protected void setTextFromString(String text) {
        currentText = new SavedTextHolder.SavedText("Unknown", "Unknown", text, SystemClock
                .elapsedRealtime());
        dbHandler.setCurrentTextTest(currentText);
        tvSpeedTest.setText(currentText.content);
    }

    @Override
    protected void setTextFromSavedText(SavedTextHolder.SavedText text) {
        currentText = text;
        dbHandler.setCurrentTextTest(currentText);
        tvSpeedTest.setText(currentText.content);
    }

    @Override
    protected SavedTextHolder.SavedText getText() {
        return currentText;
    }

    protected int getTestNum() {
        return testNumber;
    }

    @Click(R.id.scrollViewTester)
    void intercepted() {
        test();
    }

    @Click(R.id.tvSpeedTest)
    void interceptedText() {
        test();
    }

    @Click(R.id.testLayout)
    void test() {
        if (isRunning) {
            stopTesting();
        } else {
            startTesting();
        }
    }

    void startTesting() {
        if (!isAdded()) {
            return;
        }
        startTime = SystemClock.elapsedRealtime();
        isRunning = true;
        hideUiForTesting();
    }

    void stopTesting() {
        isRunning = false;
        if (!isAdded()) {
            return;
        }
        MainActivity activity = (MainActivity) getActivity();
        testLayout.setForeground(activity.obtainStyledAttributes(R.styleable.customAttrs)
                                         .getDrawable(R.styleable.customAttrs_ic_play));
        calculateAverageSpeed();
        activity.showUserSpeedDialog(averageSpeed,
                                     SavedTextHolder.DEFAULT_ITEMS,
                                     this);
        activity.showSystemUI();
    }

    @Override
    public void resetReader() {
        currentText = SavedTextHolder.DEFAULT_ITEMS.get(0);
        dbHandler.setCurrentTextTest(currentText);
        scrollToTop();
        resetSpeeds();
        testNumber = 1;
        tvSpeedTest.setText(currentText.content);
    }

    private void resetSpeeds() {
        totalSpeed = 0;
        averageSpeed = 0;
        passageSpeed = 0;
    }

    private void hideUiForTesting() {
        testLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable.customAttrs)
                                              .getDrawable(R.styleable.customAttrs_ic_pause));
        hidePauseButton(testLayout);
        ((MainActivity) getActivity()).hideSystemUI();
    }

    private int calculateAverageSpeed() {
        totalSpeed += calculatePassageSpeed();
        averageSpeed = totalSpeed / testNumber;
        return totalSpeed / testNumber;
    }

    protected double calculatePassageSpeed() {
        double readingTimeMilli = SystemClock.elapsedRealtime() - startTime;
        double milliInMin = 60000.00;
        double readingTime = readingTimeMilli / milliInMin;

        String[] words = currentText.content.split("\\s");
        int wordsRead = words.length;

        passageSpeed = wordsRead / readingTime;

        return wordsRead / readingTime; //words per minute
    }

    protected void scrollToTop() {
        scrollViewTester.fullScroll(View.FOCUS_UP);
    }

    @Touch(R.id.scrollViewTester)
    void scroll(MotionEvent event) {
        scrollViewTester.onTouchEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isRunning) {
            stopTesting();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogNumberSet(int i, int i2, double v, boolean b, double v2) {
        //Not used
    }

    @Override
    public void onUserSpeedCancelClick() {
        //Hacky way to do this, but resets speed to how it was before the current passage
        totalSpeed -= passageSpeed - 1;
        Toast.makeText(mContext, "Test Canceled",
                       Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserSpeedStopClick() {
        prefs.wpm().put(averageSpeed);
        menuSpeed.setTitle("Change Speed (" + averageSpeed + ")");
        ((MainActivity) getActivity()).showUserSpeedDialog(averageSpeed,
                                                           new ArrayList<SavedTextHolder
                                                                   .SavedText>(),
                                                           this
                                                          );
        resetSpeeds();
        Toast.makeText(mContext, "Test Stopped",
                       Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserSpeedContinueClick() {
        if (testNumber < SavedTextHolder.DEFAULT_ITEMS.size()) {
            setTextFromSavedText(SavedTextHolder.DEFAULT_ITEMS.get(testNumber));
        }
        testNumber++;
        scrollToTop();
    }

    @Override
    public void onUserSpeedOKClick() {
        //set average speed, remove dialog and reset test
        prefs.wpm().put(averageSpeed);
        menuSpeed.setTitle("Change Speed (" + averageSpeed + ")");
        resetReader();
    }

    @Override
    public void onUserSpeedStatsClick() {
        if (testNumber >= SavedTextHolder.DEFAULT_ITEMS.size()) {
            onUserSpeedOKClick();
        }
        ((MainActivity) getActivity()).replaceFragment(new ReadingStatsFragment_());
    }
}

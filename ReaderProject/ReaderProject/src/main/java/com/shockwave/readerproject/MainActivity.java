package com.shockwave.readerproject;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.shockwave.readerproject.listsections.DrawerItem;
import com.shockwave.readerproject.listsections.Sectionizer;
import com.shockwave.readerproject.listsections.SimpleSectionAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hotchemi.android.rate.AppRate;

//must add aFileChooser
@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.main)
public class MainActivity extends ActionBarActivity implements ConfirmationDialogFragment.ConfirmationDialogListener {
    //Google Play Services check
    public static final int SERVICES_REQ_CODE = 97;

    //SKU Ids for IAP
    public static final String SKU_PREMIUM = "premium_upgrade";
    public static final String SKU_ADS = "remove_ads";
    public static final String SKU_DISPLAY = "display_options";
    public static final String SKU_SPEED = "speed_increase";
    public static final String SKU_CHUNK = "chunk_increase";
    public static final String SKU_SAVE = "save_increase";

    //IAP Processor
    private BillingProcessor billingProcessor;
    private boolean readyToPurchase = false;

    //Share Provider to share app
    private ShareActionProvider mShareAppProvider;

    //ActionBar/Display Views
    private AdView adView;
    private View mDecorView;
    private ActionBarDrawerToggle mDrawerToggle;

    @ViewById(R.id.drawer_layout)
    android.support.v4.widget.DrawerLayout mDrawerLayout;

    @ViewById(R.id.left_drawer)
    ListView mDrawerList;

    @OptionsMenuItem(R.id.action_words_chunk)
    MenuItem menuWpc;

    @OptionsMenuItem(R.id.action_change_speed)
    MenuItem menuSpeed;

    @OptionsMenuItem(R.id.action_save_text)
    MenuItem menuSaveText;

    @OptionsMenuItem(R.id.action_reset)
    MenuItem menuReset;

    @OptionsMenuItem(R.id.action_change_text)
    MenuItem menuChangeText;

    @OptionsMenuItem(R.id.action_add_text)
    MenuItem menuAddText;

    @OptionsMenuItem(R.id.action_display_options)
    MenuItem menuDisplayOptions;

    @OptionsMenuItem(R.id.action_upgrade_app)
    MenuItem menuPremiumUpgrade;

    @OptionsMenuItem(R.id.action_remove_ads)
    MenuItem menuRemoveAds;

    @OptionsMenuItem(R.id.action_share_app)
    MenuItem menuShareApp;

    @Pref
    ReaderPrefs_ prefs;

    /**
     * Setup Navigation Drawer Listview
     */
    @AfterViews
    void populateList() {
        List<DrawerItem> items = new ArrayList<DrawerItem>();
        items.add(new DrawerItem("Speed Test", "Reader"));
        items.add(new DrawerItem("Chunk Reader", "Reader"));
        items.add(new DrawerItem("Word Flow", "Reader"));
        items.add(new DrawerItem("Statistics", "Other"));
        items.add(new DrawerItem("Purchases", "Other"));
        items.add(new DrawerItem("About", "Other"));

        DrawerItemAdapter adapter = new DrawerItemAdapter(this, R.layout.my_list_item, items);

        Sectionizer<DrawerItem> drawerSectionizer = new Sectionizer<DrawerItem>() {
            @Override
            public String getSectionTitleForItem(DrawerItem instance) {
                return instance.getType();
            }
        };
        SimpleSectionAdapter<DrawerItem> sectionAdapter = new SimpleSectionAdapter<DrawerItem>
                (this, adapter, R.layout.header, R.id.title, drawerSectionizer);

        mDrawerList.setAdapter(sectionAdapter);
    }

    /**
     * Setup Navigation Drawer Toggle
     */
    @AfterViews
    void setDrawerToggle() {
        registerDecorView();
        mDrawerList.setBackgroundColor(prefs.mThemeId().get() == R.style.AppTheme_Light ?
                                       Color.parseColor("#e7e8e9") : Color.parseColor("#111111"));
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                                                  R.string.drawer_open, R.string.drawer_close) {
            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * Helper method for {@link MainActivity#setDrawerToggle()}
     * Register decor view for use with navigation drawer toggle
     */
    protected void registerDecorView() {
        mDecorView = getWindow().getDecorView();
        mDecorView.setOnSystemUiVisibilityChangeListener(new View
                .OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    getSupportActionBar().show();
                } else {
                    getSupportActionBar().hide();
                }
            }
        });
    }

    /**
     * Setup ads if applicable
     */
    @AfterViews
    void setupAds() {
        if (prefs.isPremium().get() || prefs.noAds().get()) {
            ((RelativeLayout) findViewById(R.id.layout_container)).removeView(findViewById(R.id.ad_view));
        } else {
            adView = (AdView) findViewById(R.id.ad_view);

            AdRequest adRequest = new AdRequest.Builder().build();

            adView.loadAd(adRequest);
        }
    }

    /**
     * Helper method for In-App Billing/Licensing
     * Construct Encoded public key from substrings for more security
     * Key used for IAB/Licensing
     *
     * @return Base 64 Encoded Public Key for the application
     */
    private String constructEncodedPublicKey() {
        StringBuilder builder = new StringBuilder();
        builder.append("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvLiAeEa+fLfsN");
        builder.append("ju4PBfp3ohz5HHSD9sEhfPfETM0EKuxKX0kHNY4MiJNe2XCKRO8/Q4T/Ei");
        builder.append("kJ7wY1m9HL6DY8LWv3MImVCId35lKxLI7FfOeVivxoKwA0N0tCBS6CGM");
        builder.append("RVMcVsk0Y6RPFTOaRRxRs+XEPLpZd02wTEUk2kVEie/3rE6+IoQPvngO");
        builder.append("q4rOGCGwWldMHssO2gGF2seLLPG1Y8xIVQuU++3JvWXNLTSvF6TKVIz");
        builder.append("YiMQxCUBQeMcymBziP0w/55v6DoDonljgrfZNhd57OiWbLw7lsy6Fk");
        builder.append("frCjfIhy4U8h8WGJ/w+wARIk64rHgC3WgfGPBzViZzgNb3AjuQIDAQAB");
        return builder.toString();
    }

    /**
     * Setup In-App Billing
     */
    void setupIAB() {
        String base64EncodedPublicKey = constructEncodedPublicKey();
        billingProcessor = new BillingProcessor(this, base64EncodedPublicKey, new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(String productId) {
                onProductUnlocked(productId);
                if (productId.equals(SKU_SAVE) || productId.equals(SKU_SPEED) || productId.equals(SKU_CHUNK)) {
                    billingProcessor.consumePurchase(productId);
                }
            }

            @Override
            public void onPurchaseHistoryRestored() {
                for (String sku : billingProcessor.listOwnedProducts()) {
                    onProductPurchased(sku);
                }
            }

            @Override
            public void onBillingError(int errorCode, Throwable error) {

            }

            @Override
            public void onBillingInitialized() {
                readyToPurchase = true;
            }
        });
    }

    /**
     * IAB helper method to show dialog asking user to either unlock with shares or purchase the requested item
     *
     * @param sku the SKU the SKU id of the product
     */
    protected void showPurchaseDialog(String sku) {
        if (sku.equals(SKU_PREMIUM)) {
            purchaseIfReady(sku);
        } else {
            UnlockDialogFragment unlockDialogFragment = UnlockDialogFragment_.newInstance(sku);
            unlockDialogFragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
        }
    }

    /**
     * Purchases sku when called from {@link com.shockwave.readerproject.UnlockDialogFragment}
     *
     * @param sku the SKU id of the product
     */
    protected void purchaseIfReady(String sku) {
        if (!readyToPurchase) {
            Toast.makeText(this, "Billing not yet initialized", Toast.LENGTH_SHORT).show();
        } else {
            billingProcessor.purchase(sku);
        }
    }

    /**
     * Helper method for IAB
     * Checks whether a given item is purchased
     *
     * @param sku the SKU id of the product
     * @return whether the sku is purchased
     */
    protected boolean isPurchased(String sku) {
        return billingProcessor != null && billingProcessor.isPurchased(sku);
    }

    /**
     * IAB helper method for what to do when an item is purchased/unlocked.
     * Called from {@link MainActivity#setupIAB()} Billing Handler onProductPurchased
     *
     * @param productId the SKU id of the item being purchased/unlocked
     */
    public void onProductUnlocked(String productId) {
        if (productId.equals(SKU_PREMIUM)) {
            prefs.isPremium().put(true);
            prefs.noAds().put(true);
            prefs.hasDisplayOptions().put(true);
            setupAds();
        } else if (productId.equals(SKU_ADS)) {
            prefs.noAds().put(true);
            setupAds();
        } else if (productId.equals(SKU_DISPLAY)) {
            prefs.hasDisplayOptions().put(true);
        } else if (productId.equals(SKU_SAVE)) {
            prefs.saveLimit().put(prefs.saveLimit().get() + 5);
        } else if (productId.equals(SKU_SPEED)) {
            prefs.speedLimit().put(prefs.speedLimit().get() + 250);
        } else if (productId.equals(SKU_CHUNK)) {
            prefs.chunkLimit().put(prefs.chunkLimit().get() + 1);
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        if (fragment instanceof PurchasesListFragment) {
            ((PurchasesListFragment) fragment).maybeRemoveItem(productId);
        }
    }

    /**
     * IAB helper method to restore previously purchased items (called from {@link com.shockwave.readerproject.PurchasesListFragment#restorePurchases()})
     */
    protected void restorePurchases() {
        billingProcessor.loadOwnedPurchasesFromGoogle();
    }

    /**
     * IAB Helper method to get the price of a product
     *
     * @param sku the SKU id of the product
     * @return String representing the price of the the input parameter product, or empty string if could not be found
     */
    public String getPriceOfSku(String sku) {
        try {
            ArrayList<String> skuList = new ArrayList<String>();
            skuList.add(sku);
            Bundle querySkus = new Bundle();
            querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
            Bundle skuDetails = getBillingServiceField().getSkuDetails(3, getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                for (String thisResponse : responseList) {
                    JSONObject object = new JSONObject(thisResponse);
                    String productSKU = object.getString("productId");
                    if (sku.equals(productSKU)) {
                        String price = object.getString("price");
                        if (!price.isEmpty()) {
                            return price;
                        }
                    }

                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return NumberFormat.getCurrencyInstance().format(.99);
    }

    /**
     * Helper Method for {@link com.shockwave.readerproject.MainActivity#getPriceOfSku(String)}
     *
     * @return BillingService from which retrieve SKU Details
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public IInAppBillingService getBillingServiceField() throws NoSuchFieldException, IllegalAccessException {
        Field field = BillingProcessor.class.getDeclaredField("billingService");
        field.setAccessible(true);
        return (IInAppBillingService) field.get(billingProcessor);
    }

    /**
     * Helper method to change title of app as necessary
     *
     * @param f  The primary shown fragment
     * @param lf If split pane with saved texts, the saved texts list fragment
     */
    private void changeTitle(Fragment f, Fragment lf) {
        String fragName = "";
        if (f != null) {
            fragName = f.getClass().getName();
        }
        String title = "";

        ReaderDatabaseHandler dbHandler = new ReaderDatabaseHandler(this);

        if (fragName.equals(SpeedTestFragment_.class.getName())) {
            title = "Speed Test";
        } else if (fragName.equals(ChunkReaderFragment_.class.getName())) {
            title = "Chunk Reader";
        } else if (fragName.equals(WordFlowFragment_.class.getName())) {
            title = "Word Flow";
        } else if (fragName.equals(AboutFragment_.class.getName())) {
            title = "About";
        } else if (fragName.equals(ReadingStatsFragment_.class.getName())) {
            title = "Statistics";
        } else if (fragName.equals(SavedTextFragment_.class.getName())) {
            title = "Saved Texts " + showTextLimit(dbHandler);
        } else if (fragName.equals(PurchasesListFragment_.class.getName())) {
            title = "Purchases";
        }
        if (lf != null) {
            title += "/Saved Texts " + showTextLimit(dbHandler);
        }
        dbHandler.close();

        setTitle(title);
    }

    private String showTextLimit(ReaderDatabaseHandler dbHandler) {
        if (prefs.isPremium().get()) {
            return "";
        } else {
            return "(" + dbHandler.getAllTexts().size() + "/" + prefs.saveLimit().get() + ")";
        }
    }

    /**
     * Helper method for {@link com.shockwave.readerproject.MainActivity#onCreate(android.os.Bundle)}
     * Prompts user to rate after certain amount of time has passed
     */
    private void promptToRate() {
        AppRate.with(this)
               .monitor();
        AppRate.showRateDialogIfMeetsConditions(this);
    }

    /**
     * Standard onCreate method
     * Set current theme as applicable
     * Setup Backstack listener to set actionbar title
     * Setup fragment to show if fresh create
     *
     * @param savedInstanceState stores information saved when instance state changed (orientation, etc.)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set current theme
        this.setTheme(prefs.mThemeId().get());
        getWindow().setBackgroundDrawable(new ColorDrawable(prefs.mThemeId().get() == R.style
                .AppTheme_Dark ?
                                                            Color.BLACK :
                                                            Color.parseColor("#e4e4e4")));

        //set back stack change listener (for changing actionbar title)
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager
                .OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                if (findViewById(R.id.list_fragment_layout) != null) {
                    Fragment lf = getSupportFragmentManager().findFragmentById(R.id.list_fragment_layout);
                    changeTitle(f, lf);
                }
                changeTitle(f, null);
            }
        });

        //Prompt user to rate if applicable
        promptToRate();

        //Setup IAB
        setupIAB();

        //if restoring from previous state, done (means probably just changing theme/orientation)
        if (savedInstanceState != null) {
            return;
        }

        //if no previous state, initial start. show Speed Test && if first time, Welcome
        Fragment fragment = new SpeedTestFragment_();
        if (prefs.welcome().get()) {
            //welcome user
            prefs.welcome().put(false);
            (new WelcomeFragment_()).show(getSupportFragmentManager().beginTransaction(), "dialog");
        }
        replaceFragment(fragment);
    }

    /**
     * Standard onPostCreate method
     * Sync State of Drawer Toggle
     *
     * @param savedInstanceState stores information saved when instance state changed (orientation, etc.)
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    /**
     * Standard onResume method
     * Changes title if applicable
     * Checks that Google Play Services are available on device
     * Resumes AdView
     */
    @Override
    protected void onResume() {
        super.onResume();

        //Change title as applicable
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        Fragment lf = null;
        if (findViewById(R.id.list_fragment_layout) != null) {
            lf = getSupportFragmentManager().findFragmentById(R.id.list_fragment_layout);
        }
        changeTitle(f, lf);

        //Check that Google Play Services are available on device
        int servicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (servicesAvailable != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(servicesAvailable, this,
                                                  SERVICES_REQ_CODE).show();
        }
        //restore purchases if available
        if (billingProcessor != null) {
            restorePurchases();
        }
        //AdView
        if (adView != null) {
            adView.resume();
        }
    }

    /**
     * Standard onPause method
     * Pauses AdView
     */
    @Override
    protected void onPause() {
        //AdView
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    /**
     * Standard onDestroy method
     * Destroys AdView
     * Releases IAB Processor
     */
    @Override
    protected void onDestroy() {
        //AdView
        if (adView != null) {
            adView.destroy();
        }
        //In-App Billing
        if (billingProcessor != null) {
            billingProcessor.release();
        }
        super.onDestroy();
    }

    /**
     * Updates visible menu items depending on currently open fragments. Messier than doing it in
     * each fragment separately, but easier to maintain and works better.
     *
     * @param menu the default menu of the app
     * @return default return of onPrepareOptionsMenu
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //Set items depending on purchased products/Shared application
        if (prefs.hasDisplayOptions().get()) {
            menuDisplayOptions.setTitle("Display Options");
        } else {
            menuDisplayOptions.setTitle("Unlock Display Options");
        }
        if (prefs.isPremium().get()) {
            menuPremiumUpgrade.setVisible(false);
            menuRemoveAds.setVisible(false);
            menuDisplayOptions.setTitle("Display Options");
        }
        if (prefs.noAds().get()) {
            menuRemoveAds.setVisible(false);
        }

        //Setup items visible depending on fragments visible
        if (findViewById(R.id.list_fragment_layout) != null) {
            SavedTextFragment savedTextFragment = (SavedTextFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.list_fragment_layout);
            if (savedTextFragment != null) {
                menuWpc.setVisible(false);
                menuSpeed.setVisible(false);
                menuChangeText.setVisible(false);
                menuAddText.setVisible(true);
            }
        }
        Fragment mainFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        if (mainFragment != null) {
            if (mainFragment instanceof SpeedTestFragment) {
                menuWpc.setVisible(false);
                menuSpeed.setVisible(false);
            } else if (mainFragment instanceof ReadingStatsFragment) {
                menuSpeed.setVisible(false);
                menuChangeText.setVisible(false);
                menuWpc.setVisible(false);
                menuSaveText.setVisible(false);
            } else if (mainFragment instanceof SavedTextFragment) {
                menuWpc.setVisible(false);
                menuSpeed.setVisible(false);
                menuSaveText.setVisible(false);
                menuChangeText.setVisible(false);
                menuAddText.setVisible(true);
            } else if (mainFragment instanceof AboutFragment) {
                menuReset.setVisible(false);
                menuChangeText.setVisible(false);
                menuSaveText.setVisible(false);
                menuWpc.setVisible(false);
                menuSpeed.setVisible(false);
            } else if (mainFragment instanceof PurchasesListFragment) {
                menuReset.setVisible(false);
                menuChangeText.setVisible(false);
                menuSaveText.setVisible(false);
                menuWpc.setVisible(false);
                menuSpeed.setVisible(false);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Setup Share Action Provider to share application
     *
     * @param menu the default menu of the app
     * @return default return of onCreateOptionsMenu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ReaderDatabaseHandler dbHandler = new ReaderDatabaseHandler(this);
        ArrayList<Integer> stats = dbHandler.getAllStatsList();
        dbHandler.close();
        String message;
        int speedIncrease;
        if (!stats.isEmpty()) {
            speedIncrease = stats.get(stats.size() - 1) - stats.get(0);
        } else {
            speedIncrease = 0;
        }
        if (speedIncrease > 0) {
            message = "I read " + speedIncrease + " words per minute faster now thanks to Reader Project!" +
                      "\n https://play.google.com/store/apps/details?id=" + getPackageName();
        } else {
            message = "Download Reader Project to increase your reading speed!" +
                      "\n https://play.google.com/store/apps/details?id=" + getPackageName();
        }
        mShareAppProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuShareApp);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out Reader Project");
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);


        mShareAppProvider.setOnShareTargetSelectedListener(new ShareActionProvider.OnShareTargetSelectedListener() {
            @Override
            public boolean onShareTargetSelected(ShareActionProvider shareActionProvider, Intent intent) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
                if (!prefs.lastShareDate().get().equals(timeStamp)) {
                    if (prefs.shares().get() < 3 && prefs.shares().get() > -1) {
                        prefs.lastShareDate().put(timeStamp);
                        prefs.shares().put(prefs.shares().get() + 1);
                        switch (prefs.shares().get()) {
                            case 1:
                                //Increase speed by 250
                                prefs.speedLimit().put(prefs.speedLimit().get() + 250);
                                Toast.makeText(MainActivity.this, "Max speed increased by 250 wpm!", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                //Increase Chunk Size by 1
                                prefs.chunkLimit().put(prefs.chunkLimit().get() + 1);
                                Toast.makeText(MainActivity.this, "Max chunk size increased by 1 word!", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                //Increase Save Limit by 5
                                prefs.saveLimit().put(prefs.saveLimit().get() + 5);
                                prefs.shares().put(-1);
                                Toast.makeText(MainActivity.this, "Max save limit increased by 5 texts!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } else {
                        prefs.shares().put(-1);
                        Toast.makeText(MainActivity.this, "Application Shared!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Application Shared!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        setShareIntent(shareIntent);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Helper method to set the share intent of the ShareActionProvider
     *
     * @param shareIntent the intent to set for the ShareActionProvider
     */
    private void setShareIntent(Intent shareIntent) {
        if (mShareAppProvider != null) {
            mShareAppProvider.setShareIntent(shareIntent);
        }
    }

    /**
     * Standard onConfigurationChanged method
     * Change Drawer Toggle configuration as applicable
     *
     * @param newConfig new configuration to change
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Standard onActivityResult method
     * handle result if from billingProcessor, otherwise do normal result
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Show System UI as applicable for device android version
     */
    protected void showSystemUI() {
        int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && version < Build.VERSION_CODES
                .JELLY_BEAN) {
            mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        if (version >= Build.VERSION_CODES.JELLY_BEAN) {
            mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
        getSupportActionBar().show();
    }

    /**
     * Hide System UI as applicable for device android version
     */
    protected void hideSystemUI() {
        int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && version < Build.VERSION_CODES
                .JELLY_BEAN) {
            mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager
                    .LayoutParams.FLAG_FULLSCREEN);
        }
        if (version >= Build.VERSION_CODES.JELLY_BEAN && version < Build.VERSION_CODES.KITKAT) {
            mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                             | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
        if (version >= Build.VERSION_CODES.KITKAT) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE
                                            );
        }
        getSupportActionBar().hide();
    }

    /**
     * Receive click events from navigation drawer to switch current fragment
     *
     * @param position position of the item in the drawer list clicked
     */
    @ItemClick(R.id.left_drawer)
    public void drawerListItemClicked(int position) {
        selectItem(position);
    }

    /**
     * Helper method for {@link com.shockwave.readerproject.MainActivity#drawerListItemClicked(int)}
     * Set current fragment as applicable
     *
     * @param position position of the item in the drawer list clicked
     */
    private void selectItem(int position) {
        Fragment fragment = new SpeedTestFragment_();
        switch (position) {
            case 1:
                fragment = new SpeedTestFragment_();
                break;
            case 2:
                fragment = new ChunkReaderFragment_();
                break;
            case 3:
                fragment = new WordFlowFragment_();
                break;
            case 5:
                fragment = new ReadingStatsFragment_();
                break;
            case 6:
                fragment = new PurchasesListFragment_();
                break;
            case 7:
                fragment = new AboutFragment_();
        }

        replaceFragment(fragment);

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    /**
     * Helper method for many fragments
     * Replace current fragment with new fragment and add to backstack as applicable
     *
     * @param replacement the new fragment
     */
    protected void replaceFragment(Fragment replacement) {
        if (replacement == null) {
            return;
        }

        String fragmentName = replacement.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        boolean wasPopped = manager.popBackStackImmediate(fragmentName, 0);
        if (!wasPopped) {
            if (findViewById(R.id.list_fragment_layout) != null) {
                if (fragmentName.equals(SavedTextFragment_.class.getName())) {
                    transaction.replace(R.id.list_fragment_layout, replacement);
                } else {
                    if (!(replacement instanceof ReaderFragment)) {
                        Fragment lf = manager.findFragmentById(R.id.list_fragment_layout);
                        if (lf != null) {
                            transaction.remove(lf);
                        }
                        manager.popBackStack(SavedTextFragment_.class.getName(), 0);
                    }
                    transaction.replace(R.id.fragment_layout, replacement);
                }
            } else {
                transaction.replace(R.id.fragment_layout, replacement);
            }
            transaction.addToBackStack(fragmentName);
            transaction.commit();
        }

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    /**
     * Close app if back pressed and at end of backstack
     */
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Helper method to show dialog for User Speed from {@link com.shockwave.readerproject.SpeedTestFragment}
     *
     * @param speed    the speed to show
     * @param texts    the texts that have been read
     * @param fragment the fragment instance dialog is shown from
     */
    protected void showUserSpeedDialog(int speed, ArrayList<SavedTextHolder.SavedText> texts,
                                       Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        UserSpeedFragment userSpeedFragment = UserSpeedFragment.newInstance(speed, texts);
        userSpeedFragment.setTargetFragment(fragment, ReaderFragment.REQUEST_READER_FRAG);
        userSpeedFragment.show(ft, "dialog");
    }

    /**
     * Show/Hide UI as applicable
     *
     * @param e the motion event registering the touch
     * @return the touch event of the gesture detector
     */
    @Touch(R.id.fragment_layout)
    boolean onMainTouched(MotionEvent e) {
        GestureDetector clickDetector = new GestureDetector(this, new GestureDetector
                .SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                boolean visible = mDecorView.getSystemUiVisibility() == View.VISIBLE;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    visible = (mDecorView.getSystemUiVisibility() & View
                            .SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0;
                }
                if (visible) {
                    hideSystemUI();
                } else {
                    showSystemUI();
                }
                return true;
            }
        });
        return clickDetector.onTouchEvent(e);
    }

    /**
     * Called when user confirms they want to reset current fragment
     * Resets whichever fragment is applicable
     * Messier than separate in each fragment, but advantage of keeping it all in one place
     *
     * @param fragment the confirmation dialog fragment
     */
    @Override
    public void onConfirmationPositiveClick(DialogFragment fragment) {
        mDrawerLayout.closeDrawer(mDrawerList);
        if (findViewById(R.id.list_fragment_layout) != null) {
            SavedTextFragment savedTextFragment = (SavedTextFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.list_fragment_layout);
            if (savedTextFragment != null) {
                savedTextFragment.resetTexts();
            }
        }
        Fragment mainFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        if (mainFragment != null) {
            if (mainFragment instanceof ReaderFragment) {
                ((ReaderFragment) mainFragment).resetReader();
                if (((ReaderFragment) mainFragment).getClass().getName().
                        equals(SpeedTestFragment_.class.getName())) {
                    prefs.wpm().put(300);
                }
            } else if (mainFragment instanceof ReadingStatsFragment) {
                ((ReadingStatsFragment) mainFragment).resetStats();
            } else if (mainFragment instanceof SavedTextFragment) {
                ((SavedTextFragment) mainFragment).resetTexts();
            }
        }
    }

    /**
     * Show confirmation dialog to ensure user really wants to reset
     * Results of confirmation yes handled in {@link com.shockwave.readerproject.MainActivity#onConfirmationPositiveClick(android.support.v4.app.DialogFragment)}
     */
    @OptionsItem(R.id.action_reset)
    void reset() {
        ConfirmationDialogFragment confirmation = new ConfirmationDialogFragment_();
        confirmation.show(getSupportFragmentManager().beginTransaction(), "dialog");
    }

    /**
     * Toggle drawer when home icon is clicked
     */
    @OptionsItem(android.R.id.home)
    void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            mDrawerLayout.openDrawer(mDrawerList);
        }
    }

    /**
     * Buy Premium App
     */
    @OptionsItem(R.id.action_upgrade_app)
    void upgradeApp() {
        purchaseIfReady(SKU_PREMIUM);
    }

    /**
     * Remove Ads
     */
    @OptionsItem(R.id.action_remove_ads)
    void removeAds() {
        purchaseIfReady(SKU_ADS);
    }

    /**
     * Show display options popup if accessible, otherwise prompt user to buy display options
     */
    @OptionsItem(R.id.action_display_options)
    void changeDisplayOptions() {
        if (prefs.hasDisplayOptions().get() || prefs.isPremium().get()) {
            showDisplayOptions();
        } else {
            purchaseIfReady(SKU_DISPLAY);
        }
    }

    /**
     * Helper method for {@link MainActivity#changeDisplayOptions()}
     * Show dialog with display options
     */
    private void showDisplayOptions() {
        DisplayOptionsFragment displayOptionsFragment = new DisplayOptionsFragment_();
        displayOptionsFragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
    }


    /**
     * Holder to hold navigation drawer items
     */
    static class Holder {
        public TextView itemTextView;
    }

    /**
     * Adapter to layout Navigation Drawer
     */
    class DrawerItemAdapter extends ArrayAdapter<DrawerItem> {
        private List<DrawerItem> drawerItems;

        public DrawerItemAdapter(Context context, int textViewResourceId,
                                 List<DrawerItem> drawerItems) {
            super(context, textViewResourceId, drawerItems);
            this.drawerItems = drawerItems;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            Holder holder;

            if (view == null) {
                view = View.inflate(MainActivity.this, R.layout.my_list_item, null);

                holder = new Holder();
                holder.itemTextView = (TextView) view.findViewById(R.id.list_content1);

                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            DrawerItem item = drawerItems.get(position);
            holder.itemTextView.setText(item.getName());

            return view;
        }
    }

}

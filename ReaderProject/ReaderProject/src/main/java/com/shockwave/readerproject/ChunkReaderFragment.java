package com.shockwave.readerproject;

import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Chunk Reader Fragment displaying a full passage of text with one chunk highlighted, moving that
 * chunk forward at set speed.
 * Designed to increase reader speed and comprehension with full passage context in mind.
 */
@EFragment(R.layout.fragment_chunk_reader)
public class ChunkReaderFragment extends ReaderFragment {
    private Handler highlightTextHandler;
    private Runnable highlightText;

    private SpannableStringBuilder str;
    private ForegroundColorSpan colorSpan;
    private Pattern p;

    @InstanceState
    int start = 0;

    @InstanceState
    int end = 0;

    @InstanceState
    int oldSpanSize;

    @InstanceState
    SavedTextHolder.SavedText currentText;

    @ViewById
    TextView tvChunkReader;

    @ViewById
    ScrollView scrollViewReader;

    @ViewById
    FrameLayout chunkLayout;

    @AfterViews
    void setText() {
        tvChunkReader.setTextSize(prefs.fontSizeGeneral().get());
        highlightTextHandler = new Handler();
        highlightText = new Runnable() {
            @Override
            public void run() {
                if (highlightChunk()) {
                    highlightTextHandler.postDelayed(highlightText,
                                                     1000 / getWordsPerSec(prefs.wpm().get()));
                }
            }
        };
        updateViews();
        if (isRunning) {
            startReading(highlightTextHandler, highlightText, prefs.wpm().get());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isRunning) {
            stopReading(highlightTextHandler, highlightText);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    void selectText() {
        stopReading(highlightTextHandler, highlightText);
        super.selectText();
    }

    @Override
    protected void setTextFromString(String text) {
        currentText = new SavedTextHolder.SavedText("Unknown", "Unknown", text, SystemClock
                .elapsedRealtime());
        dbHandler.setCurrentTextChunk(currentText);
        start = 0;
        updateViews();
    }

    @Override
    protected void setTextFromSavedText(SavedTextHolder.SavedText text) {
        currentText = text;
        dbHandler.setCurrentTextChunk(currentText);
        start = 0;
        updateViews();
    }

    @Override
    protected SavedTextHolder.SavedText getText() {
        return currentText;
    }

    /**
     * used only if text or theme is changed, and when fragment first starts
     */
    private void updateViews() {
        if (currentText == null) {
            currentText = dbHandler.getCurrentText(CURRENT_TEXT_KEY_CHUNK);
        }
        str = new SpannableStringBuilder(currentText.content);
        if (p == null) {
            p = Pattern.compile("\\s|\\z");
        }

        if (start == 0) {
            scrollViewReader.fullScroll(View.FOCUS_UP);
            end = updateEnd();
        } else {
            //theme changed, set start and end back one chunk to prevent skipping forward
            end = start - 1;
            start -= oldSpanSize;
            if (start < 0) {
                start = 0;
            }
        }

        tvChunkReader.setTextColor(prefs.mThemeId().get() == R.style.AppTheme_Light ? Color.LTGRAY
                                                                                    : Color.DKGRAY);
        colorSpan = new ForegroundColorSpan(prefs.mThemeId().get() == R.style.AppTheme_Light ?
                                            Color.BLACK : Color.WHITE);

        highlightChunk();
    }

    /**
     * Updates end of text chunk according to how big chunk size is and where the next chunk is
     *
     * @return redundant return, end is instance variable set in the method, no need to return
     * ^^Why am I even doing that?
     */
    private int updateEnd() {
        Matcher m = p.matcher(str.subSequence(start, str.length()));
        for (int ind = 0; ind < prefs.wpc().get(); ind++) {
            if (m.find()) {
                end = start + m.start();
            }
        }
        return end;
    }

    /**
     * General method to highlight a chunk of text. Sets a text span of chunk size on the TextView text
     * Very inefficient, especially for large texts, because it sets the TextView text every time
     *
     * @return whether the reader is running (whether to keep highlighting)
     */
    private boolean highlightChunk() {
        if (start == 1) {
            start = 0;
        }

        str.setSpan(colorSpan, start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        tvChunkReader.setText(str);
        oldSpanSize = end - start + 1;

        if (str.getSpanEnd(colorSpan) < str.length()) {
            start = str.getSpanEnd(colorSpan) + 1;
        } else {
            //reached end of text
            str.removeSpan(colorSpan);
            scrollViewReader.fullScroll(View.FOCUS_UP);
            start = 0;
            end = updateEnd();
            isRunning = false;
            stopReading(highlightTextHandler, highlightText);
        }

        end = updateEnd(); //updates so next time that span is set it is a word (or chunk) forward

        return isRunning;
    }

    @Override
    protected void startReading(Handler handler, Runnable runnable, int wordsPerMin) {
        super.startReading(handler, runnable, wordsPerMin);

        if (!isAdded()) {
            return;
        }
        chunkLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable.customAttrs)
                                               .getDrawable(R.styleable.customAttrs_ic_pause));
        hidePauseButton(chunkLayout);
    }

    @Override
    protected void stopReading(Handler handler, Runnable runnable) {
        super.stopReading(handler, runnable);
        if (!isAdded()) {
            return;
        }

        if (!isRunning) {
            chunkLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable
                                                                                   .customAttrs)
                                                   .getDrawable(R.styleable.customAttrs_ic_play));
        } else {
            chunkLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable
                                                                                   .customAttrs)
                                                   .getDrawable(R.styleable.customAttrs_ic_pause));
            hidePauseButton(chunkLayout);
        }
    }

    @Override
    public void resetReader() {
        str.removeSpan(colorSpan);
        scrollViewReader.fullScroll(View.FOCUS_UP);
        setWordsPerChunk(1);
        setWordsPerMin(prefs.wpm().get());
        start = 0;
        end = updateEnd();
        highlightChunk();
    }

    @Click(R.id.scrollViewReader)
    void intercepted() {
        highlight();
    }

    @Click(R.id.tvChunkReader)
    void interceptedText() {
        highlight();
    }

    @Click(R.id.chunkLayout)
    void highlight() {
        if (isRunning) {
            if (!pauseClick) {
                ((MainActivity) getActivity()).showSystemUI();
                hideUIOnDelay((MainActivity) getActivity());
                pauseClick = true;
            } else {
                stopReading(highlightTextHandler, highlightText);
                pauseClick = false;
            }
        } else {
            startReading(highlightTextHandler, highlightText, prefs.wpm().get());
        }
    }

    @Override
    public void onDialogNumberSet(int reference, int number, double decimal, boolean isNegative,
                                  double fullNumber) {
        switch (reference) {
            case WPC_REFERENCE:
                setWordsPerChunk(number);
                break;
            case SPEED_REFERENCE:
                setWordsPerMin(number);
                break;
        }
    }

    @Override
    void changeWordsPerChunk() {
        stopReading(highlightTextHandler, highlightText);
        super.changeWordsPerChunk();
    }

    @Override
    protected void setWordsPerChunk(int wpc) {
        super.setWordsPerChunk(wpc);
        start = str.getSpanStart(colorSpan);
        if (start < 0) {
            start = 0;
        }
        end = updateEnd();
        highlightChunk();
    }

}

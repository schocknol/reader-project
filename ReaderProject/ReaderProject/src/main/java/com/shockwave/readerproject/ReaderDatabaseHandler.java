package com.shockwave.readerproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewDataInterface;
import com.jjoe64.graphview.GraphViewSeries;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.shockwave.readerproject.SavedTextHolder.SavedText;

/**
 * Database Handler to store current texts, saved texts, and statistics
 * All in same database (3 separate tables) because it's easier to maintain than 3 separate databases
 * Created by Nolan Schock, 2014
 */
@EBean
public class ReaderDatabaseHandler extends SQLiteOpenHelper {
    //General Database Info
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "textsAndStats";

    //Current Texts Table
    private static final String TABLE_CURRENT_TEXTS = "current_texts";

    //Saved Texts Table
    private static final String TABLE_TEXTS = "texts";

    //Current Texts/Saved Texts Tables
    private static final String KEY_TIME_CREATED = "time";
    private static final String KEY_TITLE = "title";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_CONTENT = "content";

    //Stats Table
    private static final String TABLE_GRAPH_DATA = "stats";

    private static final String KEY_DATE = "date";
    private static final String KEY_SPEED = "speed";

    public ReaderDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //current texts table
        String CREATE_CURRENT_TEXTS_TABLE = "CREATE TABLE " + TABLE_CURRENT_TEXTS + "("
                                            + KEY_TIME_CREATED + " INTEGER PRIMARY KEY,"
                                            + KEY_TITLE + " TEXT," + KEY_AUTHOR + " TEXT,"
                                            + KEY_CONTENT + " TEXT"
                                            + ")";
        db.execSQL(CREATE_CURRENT_TEXTS_TABLE);

        //texts table
        String CREATE_TEXT_TABLE = "CREATE TABLE " + TABLE_TEXTS + "("
                                   + KEY_TIME_CREATED + " INTEGER PRIMARY KEY,"
                                   + KEY_TITLE + " TEXT," + KEY_AUTHOR + " TEXT,"
                                   + KEY_CONTENT + " TEXT"
                                   + ")";
        db.execSQL(CREATE_TEXT_TABLE);
        if (isTextsEmpty(db)) {
            initDefaults(db);
        }

        //stats table
        String CREATE_STATS_TABLE = "CREATE TABLE " + TABLE_GRAPH_DATA + "("
                                    + KEY_DATE + " INTEGER PRIMARY KEY,"
                                    + KEY_SPEED + " INTEGER"
                                    + ")";
        db.execSQL(CREATE_STATS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CURRENT_TEXTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEXTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GRAPH_DATA);

        onCreate(db);
    }

    /**
     * Add default texts to Saved Texts Table if they're not already there
     */
    private void initDefaults(SQLiteDatabase db) {
        for (int i = 0; i < SavedTextHolder.DEFAULT_ITEMS.size(); i++) {
            SavedText text = SavedTextHolder.DEFAULT_ITEMS.get(i);
            ContentValues values = new ContentValues();
            values.put(KEY_TIME_CREATED, text.timeCreated);
            values.put(KEY_TITLE, text.title);
            values.put(KEY_AUTHOR, text.author);
            values.put(KEY_CONTENT, text.content);

            db.insert(TABLE_TEXTS, null, values);
        }
    }

    /**
     * Current Text Table Operations
     */

    public void setCurrentTextTest(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME_CREATED, ReaderFragment.CURRENT_TEXT_KEY_TEST);
        values.put(KEY_TITLE, text.title);
        values.put(KEY_AUTHOR, text.author);
        values.put(KEY_CONTENT, text.content);

        db.replace(TABLE_CURRENT_TEXTS, null, values);
        db.close();
    }

    public void setCurrentTextChunk(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME_CREATED, ReaderFragment.CURRENT_TEXT_KEY_CHUNK);
        values.put(KEY_TITLE, text.title);
        values.put(KEY_AUTHOR, text.author);
        values.put(KEY_CONTENT, text.content);

        db.replace(TABLE_CURRENT_TEXTS, null, values);
        db.close();
    }

    public void setCurrentTextFlow(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME_CREATED, ReaderFragment.CURRENT_TEXT_KEY_FLOW);
        values.put(KEY_TITLE, text.title);
        values.put(KEY_AUTHOR, text.author);
        values.put(KEY_CONTENT, text.content);

        db.replace(TABLE_CURRENT_TEXTS, null, values);
        db.close();
    }

    public SavedText getCurrentText(long timeCreated) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CURRENT_TEXTS, new String[]{KEY_TIME_CREATED,
                                                                   KEY_TITLE, KEY_AUTHOR, KEY_CONTENT},
                                 KEY_TIME_CREATED + "=?",
                                 new String[]{String.valueOf(timeCreated)}, null, null, null, null);

        //if there is no current text saved, set a current text of the first default item
        if (cursor == null || cursor.getCount() == 0) {
            if (timeCreated == ReaderFragment.CURRENT_TEXT_KEY_TEST) {
                setCurrentTextTest(SavedTextHolder.DEFAULT_ITEMS.get(0));
                return SavedTextHolder.DEFAULT_ITEMS.get(0);
            }
            if (timeCreated == ReaderFragment.CURRENT_TEXT_KEY_CHUNK) {
                setCurrentTextChunk(SavedTextHolder.DEFAULT_ITEMS.get(0));
                return SavedTextHolder.DEFAULT_ITEMS.get(0);
            }
            if (timeCreated == ReaderFragment.CURRENT_TEXT_KEY_FLOW) {
                setCurrentTextFlow(SavedTextHolder.DEFAULT_ITEMS.get(0));
                return SavedTextHolder.DEFAULT_ITEMS.get(0);
            }
            return null;
        } else {
            cursor.moveToFirst();
        }

        SavedText text = new SavedText(cursor.getString(1),
                                       cursor.getString(2),
                                       cursor.getString(3),
                                       Long.parseLong(cursor.getString(0)));
        cursor.close();
        return text;
    }

    /**
     * Saved Texts Table Operations
     */

    public void addText(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME_CREATED, text.timeCreated);
        values.put(KEY_TITLE, text.title);
        values.put(KEY_AUTHOR, text.author);
        values.put(KEY_CONTENT, text.content);

        db.insertOrThrow(TABLE_TEXTS, null, values);
        db.close();
    }

    public SavedText getText(long timeCreated) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TEXTS, new String[]{KEY_TIME_CREATED,
                                                           KEY_TITLE, KEY_AUTHOR, KEY_CONTENT},
                                 KEY_TIME_CREATED + "=?",
                                 new String[]{String.valueOf(timeCreated)}, null, null, null, null);
        if (cursor == null || cursor.getCount() == 0) {
            return null;
        } else {
            cursor.moveToFirst();
        }

        SavedText text = new SavedText(cursor.getString(1),
                                       cursor.getString(2),
                                       cursor.getString(3),
                                       Long.parseLong(cursor.getString(0)));
        cursor.close();
        return text;
    }

    public List<SavedText> getAllTexts() {
        List<SavedText> texts = new ArrayList<SavedText>();
        //get all entries except the current texts
        String selectQuery = "SELECT  * FROM " + TABLE_TEXTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                long timeCreated = Long.parseLong(cursor.getString(0));
                String title = cursor.getString(1);
                String author = cursor.getString(2);
                String content = cursor.getString(3);
                SavedText text = new SavedText(title, author, content, timeCreated);

                texts.add(text);
            } while (cursor.moveToNext());
        }
        cursor.close();
        Collections.sort(texts);
        return texts;
    }

    public long replaceText(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME_CREATED, text.timeCreated);
        values.put(KEY_TITLE, text.title);
        values.put(KEY_AUTHOR, text.author);
        values.put(KEY_CONTENT, text.content);

        return db.replace(TABLE_TEXTS, null, values);
    }

    public void deleteText(SavedText text) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TEXTS, KEY_TIME_CREATED + " = ?",
                  new String[]{String.valueOf(text.timeCreated)});
        db.close();
    }

    public void deleteAllTexts() {
        SQLiteDatabase db = this.getWritableDatabase();
        //delete everything except the default texts
        db.delete(TABLE_TEXTS, KEY_TIME_CREATED + " !=? AND " + KEY_TIME_CREATED + " !=? AND " +
                               KEY_TIME_CREATED + " !=? ",
                  new String[]{String.valueOf(SavedTextHolder.TEXT_1_TIME),
                               String.valueOf(SavedTextHolder.TEXT_2_TIME),
                               String.valueOf(SavedTextHolder.TEXT_3_TIME)});
        //just in case, re-add default texts if they had been previously deleted
        for (SavedText text : SavedTextHolder.DEFAULT_ITEMS) {
            if (getText(text.timeCreated) == null) {
                addText(text);
            }
        }
    }

    public boolean isTextsEmpty(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_TEXTS, null);
        if (cursor != null) {
            cursor.moveToFirst();
            boolean empty = cursor.getInt(0) == 0;
            cursor.close();
            return empty;
        } else {
            return false;
        }
    }

    /**
     * Reading Stats Table Operations
     */

    public void addStat(long date, int speed) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, date);
        values.put(KEY_SPEED, speed);

        db.insert(TABLE_GRAPH_DATA, null, values);
        db.close();
    }

    /**
     * For graphing stats
     */
    public GraphViewSeries getAllStatsGraph() {
        GraphViewSeries stats = null;
        String selectQuery = "SELECT  * FROM " + TABLE_GRAPH_DATA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                long date = Long.parseLong(cursor.getString(0));
                int speed = Integer.parseInt(cursor.getString(1));
                GraphView.GraphViewData stat = new GraphView.GraphViewData(date, speed);

                if (stats == null) {
                    stats = new GraphViewSeries(new GraphViewDataInterface[]{stat});
                } else {
                    stats.appendData(stat, false, 365);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return stats;
    }

    /**
     * For sharing stats
     */
    public ArrayList<Integer> getAllStatsList() {
        ArrayList<Integer> stats = new ArrayList<Integer>();

        String selectQuery = "SELECT  * FROM " + TABLE_GRAPH_DATA;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                stats.add(Integer.parseInt(cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return stats;
    }

    public void deleteAllStats() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GRAPH_DATA, null, null);
    }
}

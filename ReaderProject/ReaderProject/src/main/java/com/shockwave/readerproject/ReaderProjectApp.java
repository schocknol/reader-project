package com.shockwave.readerproject;

import android.app.Application;
import android.content.Context;

import org.androidannotations.annotations.EApplication;

/**
 * General Application class for getting the context of the app (for access from non-Android classes
 * such as SavedText)
 * Created by Nolan Schock, 2014
 */
@EApplication
public class ReaderProjectApp extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}

package com.shockwave.readerproject;

import android.app.backup.BackupAgentHelper;
import android.app.backup.FileBackupHelper;
import android.app.backup.SharedPreferencesBackupHelper;

/**
 * Backup Agent Helper to backup Shared Preferences and SQL Database
 * Created by Nolan Schock, 2014
 */
public class ReaderBackupAgent extends BackupAgentHelper {
    // The name of the SharedPreferences file
    static final String PREFS = "ReaderPrefs";

    // A key to uniquely identify the set of backup data for the sharedprefs
    static final String PREFS_BACKUP_KEY = "reader_prefs";

    // The name of the db file
    static final String READER_DATABASE = ReaderDatabaseHandler.DATABASE_NAME;

    // A key to uniquely identify the set of backup data for the database
    static final String DATABASE_BACKUP_KEY = "dbfiles";

    // Allocate a helper and add it to the backup agent
    @Override
    public void onCreate() {
        SharedPreferencesBackupHelper prefsHelper = new SharedPreferencesBackupHelper(this, PREFS);
        addHelper(PREFS_BACKUP_KEY, prefsHelper);

        FileBackupHelper dbHelper = new FileBackupHelper(this, "../databases/" + READER_DATABASE);
        addHelper(DATABASE_BACKUP_KEY, dbHelper);
    }
}

package com.shockwave.readerproject;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.jensdriller.libs.undobar.UndoBar;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.List;

/**
 * Fragment containing list of Saved Texts
 * Created by Nolan Schock, 2014
 */
@EFragment
public class SavedTextFragment extends ListFragment implements
                                                    ChangeTextInfoFragment
                                                            .ChangeTextInfoListener {
    public static final int SAVED_TEXT_REQ_CODE = 123;

    private OnSavedTextSelectedListener mCallback;
    ListView mListView;
    SavedTextAdapter mAdapter;

    private ReaderDatabaseHandler dbHandler;

    @Pref
    ReaderPrefs_ prefs;

    @OptionsMenuItem(R.id.action_add_text)
    MenuItem menuAddText;

    @OptionsItem(R.id.action_add_text)
    void addText() {
        if (!prefs.isPremium().get()) {
            //save limit reached. query if user wants to buy more saves
            if (dbHandler.getAllTexts().size() >= prefs.saveLimit().get()) {
                MainActivity activity = (MainActivity) getActivity();
                activity.showPurchaseDialog(MainActivity.SKU_SAVE);
                return;
            }
        }
        //if is premium or limit not reached show user dialog to add new text
        ChangeTextInfoFragment fragment = ChangeTextInfoFragment.newInstance(new SavedTextHolder
                .SavedText("Unknown", "Unknown", "", -1));
        fragment.setTargetFragment(this, SAVED_TEXT_REQ_CODE);
        fragment.show(getFragmentManager().beginTransaction(), "dialog");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //carry out action in OnSavedTextSelectedListener (Reader) meaning set current text as applicable
        SavedTextHolder.SavedText text = ((SavedTextAdapter) getListAdapter()).getItem(position);
        mCallback.onSavedTextSelected(text);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //initialize database handler and Reader Callback
        dbHandler = new ReaderDatabaseHandler(activity);
        try {
            mCallback = (OnSavedTextSelectedListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment().toString() + "must implement " +
                                         "OnSavedTextSelectedListener");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView = getListView();

        List<SavedTextHolder.SavedText> texts = dbHandler.getAllTexts();

        setListAdapter(new SavedTextAdapter(getActivity(), R.layout.my_two_line_list_item, texts));
        mAdapter = (SavedTextAdapter) getListAdapter();

        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        mListView,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                //pos and saved text determined in loop and used for undo
                                int pos = 0;
                                SavedTextHolder.SavedText item = mAdapter.getItem(pos);
                                for (int position : reverseSortedPositions) {
                                    pos = position;
                                    item = mAdapter.getItem(position);
                                    mAdapter.remove(mAdapter.getItem(position));
                                }
                                mAdapter.notifyDataSetChanged();

                                UndoBar bar = new UndoBar(getActivity());
                                bar.setMessage("Text deleted.");
                                final int finalPos = pos;
                                final SavedTextHolder.SavedText finalItem = item;
                                bar.setListener(new UndoBar.Listener() {
                                    @Override
                                    public void onHide() {
                                        dbHandler.deleteText(finalItem);
                                    }

                                    @Override
                                    public void onUndo(Parcelable token) {
                                        mAdapter.insert(finalItem, finalPos);
                                    }
                                });
                                bar.show();

                            }
                        });
        mListView.setOnTouchListener(touchListener);
        mListView.setOnScrollListener(touchListener.makeScrollListener());

        registerForContextMenu(mListView);
        setupContextualActionMenu();
    }

    /**
     * Context menu for when items are long clicked
     * Actions to delete, edit, and select all items
     */
    private void setupContextualActionMenu() {
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            MenuItem menuEdit;

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                                  boolean checked) {
                if (checked) {
                    mAdapter.addSelection(position);
                } else {
                    mAdapter.removeSelectedItem(position);
                }
                if (mAdapter.getSelectionSize() > 1) {
                    menuEdit.setVisible(false);
                } else {
                    menuEdit.setVisible(true);
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context, menu);
                menuEdit = menu.findItem(R.id.action_edit);
                mAdapter.resetSelected();
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        final List<SavedTextHolder.SavedText> selected = mAdapter.getSelected();
                        mAdapter.removeSelected();
                        new UndoBar.Builder(getActivity())
                                .setMessage(selected.size() + " text(s) deleted.")
                                .setListener(new UndoBar.Listener() {
                                    @Override
                                    public void onHide() {
                                        for (SavedTextHolder.SavedText text : selected) {
                                            dbHandler.deleteText(text);
                                        }
                                        mAdapter.resetSelected();
                                    }

                                    @Override
                                    public void onUndo(Parcelable token) {
                                        mAdapter.addAll(selected);
                                        mAdapter.sort(new SavedTextHolder.SavedTextComparator());
                                        mAdapter.notifyDataSetChanged();
                                    }
                                })
                                .show();
                        mode.finish();
                        return true;
                    case R.id.action_edit:
                        SavedTextHolder.SavedText oldText = mAdapter.getSelection(0);
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ChangeTextInfoFragment fragment = ChangeTextInfoFragment_.newInstance
                                (oldText);
                        fragment.setTargetFragment(SavedTextFragment.this, SAVED_TEXT_REQ_CODE);
                        fragment.show(ft, "dialog");
                        mode.finish();
                        return true;
                    case R.id.action_select_all:
                        if (mListView.getCheckedItemCount() >= mListView.getCount()) {
                            for (int i = 0; i < mListView.getCount(); i++) {
                                mListView.setItemChecked(i, false);
                            }
                        } else {
                            for (int i = 0; i < mListView.getCount(); i++) {
                                if (!mListView.isItemChecked(i)) {
                                    mListView.setItemChecked(i, true);
                                }
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }
        });
    }

    protected void resetTexts() {
        //Delete everything from handler and adapter except default texts (and add those if previously deleted)
        dbHandler.deleteAllTexts();
        mAdapter.resetAll();
        mAdapter.notifyDataSetChanged();
        mListView.invalidateViews();
    }

    @Override
    public void onChangeTextPositiveClick(final SavedTextHolder.SavedText newText,
                                          final SavedTextHolder.SavedText oldText) {
        if (!prefs.isPremium().get()) {
            if (dbHandler.getAllTexts().size() >= prefs.saveLimit().get()) {
                //save limit reached. query if user wants to buy more saves
                MainActivity activity = (MainActivity) getActivity();
                activity.showPurchaseDialog(MainActivity.SKU_SAVE);
                return;
            }
        }
        //either premium or save limit not reached, so add new text/edit old text as applicable
        mAdapter.add(newText);
        mAdapter.sort(new SavedTextHolder.SavedTextComparator());
        if (oldText.timeCreated == -1) {
            //timeCreated == -1 indicates no old text, adding new text.
            Toast.makeText(getActivity(), "Text added.", Toast.LENGTH_SHORT).show();
            dbHandler.addText(newText);
        } else {
            //Changing old text.
            mAdapter.remove(oldText);
            UndoBar bar = new UndoBar(getActivity());
            bar.setMessage("Text updated.");
            bar.setListener(new UndoBar.Listener() {
                @Override
                public void onHide() {
                    dbHandler.replaceText(newText);
                }

                @Override
                public void onUndo(Parcelable token) {
                    mAdapter.remove(newText);
                    if (oldText.timeCreated != -1) {
                        mAdapter.add(oldText);
                    }
                    mAdapter.sort(new SavedTextHolder.SavedTextComparator());
                }
            });
            bar.show();
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dbHandler.close();
    }

    public interface OnSavedTextSelectedListener {
        public void onSavedTextSelected(SavedTextHolder.SavedText text);
    }

}

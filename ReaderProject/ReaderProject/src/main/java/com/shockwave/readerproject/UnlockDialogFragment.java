package com.shockwave.readerproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.text.NumberFormat;

/**
 * Dialog fragment prompting user to buy more saves/increase speed/chunk size when limit reached
 * Created by Nolan Schock, 2014
 */
@EFragment
public class UnlockDialogFragment extends DialogFragment {
    private MainActivity activity;

    @InstanceState
    String price;

    @Pref
    ReaderPrefs_ prefs;


    static UnlockDialogFragment newInstance(String purchase) {
        UnlockDialogFragment fragment = new UnlockDialogFragment_();

        Bundle args = new Bundle();
        args.putString("purchaseId", purchase);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = (MainActivity) getActivity();
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String purchaseId = getArguments().getString("purchaseId");
        if (savedInstanceState == null) {
            price = activity.getPriceOfSku(purchaseId);
        }
        if (price == null) {
            price = NumberFormat.getCurrencyInstance().format(.99);
        }
        String message;
        if (purchaseId.equals(MainActivity.SKU_SPEED)) {
            message = "Max speed reached. Increase max speed by 250 wpm?";
        } else if (purchaseId.equals(MainActivity.SKU_CHUNK)) {
            message = "Chunk size limit reached. Increase max chunk size by 1 word?";
        } else if (purchaseId.equals(MainActivity.SKU_SAVE)) {
            message = "Save limit reached. Increase max saved texts by 5 texts?";
        } else {
            message = "Purchase Item?";
        }
        return new AlertDialog.Builder(getActivity())
                .setTitle("Unlock?")
                .setMessage(message)
                .setPositiveButton("Buy (" + price + ")", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.purchaseIfReady(purchaseId);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }
}

package com.shockwave.readerproject;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Interface storing all Shared Preferences, typesafe, through Android Annotations
 * Created by Nolan Schock, 2014
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface ReaderPrefs {

    @DefaultBoolean(true)
    boolean welcome();

    @DefaultInt(R.style.AppTheme_Light)
    int mThemeId();

    @DefaultInt(300)
    int wpm();

    @DefaultInt(1)
    int wpc();

    @DefaultInt(16)
    int fontSizeGeneral();

    @DefaultInt(40)
    int fontSizeFlow();

    @DefaultBoolean(false)
    boolean isPremium();

    @DefaultBoolean(false)
    boolean noAds();

    @DefaultBoolean(false)
    boolean hasDisplayOptions();

    @DefaultInt(10)
    int saveLimit();

    @DefaultInt(500)
    int speedLimit();

    @DefaultInt(2)
    int chunkLimit();

    @DefaultInt(0)
    int shares();

    @DefaultString("00000000")
    String lastShareDate();
}


package com.shockwave.readerproject;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Class to hold Saved Text objects in the default list
 * Created by Nolan Schock, 2014
 */
@EBean
public class SavedTextHolder {
    public static final int TEXT_1_TIME = 0;
    public static final int TEXT_2_TIME = 1;
    public static final int TEXT_3_TIME = 2;
    public static ArrayList<SavedText> DEFAULT_ITEMS = new ArrayList<SavedText>();

    static {
        // Add 3 sample items.
        addItem(new SavedText("A Study in Scarlet (Sample)", "Arthur Conan Doyle",
                              R.string.default_text_3, TEXT_1_TIME));
        addItem(new SavedText("Moby Dick (Sample)", "Herman Melville", R.string.default_text_1,
                              TEXT_2_TIME));
        addItem(new SavedText("Alice's Adventures in Wonderland (Sample)", "Lewis Carroll",
                              R.string.default_text_2, TEXT_3_TIME));
        Collections.sort(DEFAULT_ITEMS);
    }

    protected static void addItem(SavedText item) {
        DEFAULT_ITEMS.add(item);
    }

    /**
     * Saved Text class, used throughout application to store texts the user would like to read
     * Created by Nolan Schock, 2014
     */
    public static class SavedText implements Parcelable, Comparable<SavedText> {
        public String title;
        public String author;
        public String content;
        public long timeCreated; //identifier to distinguish between otherwise identical texts

        public SavedText() {
            this.title = "Unknown";
            this.author = "Unknown";
            this.content = "";
            this.timeCreated = -1;
        }

        public SavedText(String title, String author, String content, long timeCreated) {
            this.title = title;
            this.author = author;
            this.content = content;
            this.timeCreated = timeCreated;
        }

        public SavedText(String title, String author, int resId, long timeCreated) {
            this.title = title;
            this.author = author;
            this.content = ReaderProjectApp.getContext().getString(resId);
            this.timeCreated = timeCreated;
        }

        public SavedText(Parcel in) {
            readFromParcel(in);
        }

        public static final Creator<SavedText> CREATOR = new Creator<SavedText>() {
            @Override
            public SavedText createFromParcel(Parcel source) {
                return new SavedText(source);
            }

            @Override
            public SavedText[] newArray(int size) {
                return new SavedText[size];
            }
        };

        private void readFromParcel(Parcel in) {
            title = in.readString();
            author = in.readString();
            content = in.readString();
            timeCreated = in.readLong();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(author);
            dest.writeString(content);
            dest.writeLong(timeCreated);
        }

        /**
         * Only compare title and author, for listview position sorting only
         */
        @Override
        public int compareTo(@NonNull SavedText another) {
            int authorComp = this.author.compareTo(another.author);
            int titleComp = this.title.compareTo(another.title);
            if (authorComp == 0) {
                if (titleComp == 0) {
                    return this.content.compareTo(another.content);
                } else {
                    return titleComp;
                }
            }
            return authorComp;
        }

        @Override
        public boolean equals(Object o) {
            //overridden to ignore hashcode
            return o != null && o.getClass() == this.getClass() &&
                   this.timeCreated == ((SavedText) o).timeCreated &&
                   this.title.equals(((SavedText) o).title)
                   && this.author.equals(((SavedText) o).author)
                   && this.content.equals(((SavedText) o).content);
        }

        @Override
        public String toString() {
            return title + "\n" + author + "\n" + content;
        }
    }

    /**
     * Saved Text Comparator class, used for sorting algorithm
     */
    public static class SavedTextComparator implements Comparator<SavedText> {

        @Override
        public int compare(SavedText lhs, SavedText rhs) {
            return lhs.compareTo(rhs);
        }
    }
}

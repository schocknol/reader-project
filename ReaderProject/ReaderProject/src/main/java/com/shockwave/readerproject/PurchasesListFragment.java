package com.shockwave.readerproject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Purchases List Fragment displaying list of items user may purchase and a button allowing user to
 * manually restore purchases (if for some reason they didn't automatically restore)
 * Created by Nolan Schock, 2014
 */
@EFragment(R.layout.fragment_purchases)
public class PurchasesListFragment extends ListFragment {
    private MainActivity activity;

    @Pref
    ReaderPrefs_ prefs;

    @ViewById
    TextView tvNoPurchases;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_purchases, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<HashMap<String, String>> purchasesList = new ArrayList<HashMap<String, String>>();

        maybeAddItem(MainActivity.SKU_SPEED, purchasesList, "Increase Custom Speed by 250 (1st Share)",
                     "(Current: " + limitOrPremium(prefs.speedLimit().get()) + ") (Repeatable)");

        maybeAddItem(MainActivity.SKU_CHUNK, purchasesList, "Increase Chunk Size by 1 (2nd Share)",
                     "(Current: " + limitOrPremium(prefs.chunkLimit().get()) + ") (Repeatable)");

        maybeAddItem(MainActivity.SKU_SAVE, purchasesList, "Increase Save Limit by 5 (3rd Share)",
                     "(Current: " + limitOrPremium(prefs.saveLimit().get()) + ") (Repeatable)");

        maybeAddItem(MainActivity.SKU_DISPLAY, purchasesList, "Unlock Display Options (Must Purchase)",
                     "(Night Theme and Adjustable Font Sizes)");

        maybeAddItem(MainActivity.SKU_ADS, purchasesList, "Remove Ads (Must Purchase)",
                     "(Permanently remove banner ads)");

        maybeAddItem(MainActivity.SKU_PREMIUM, purchasesList, "Premium Upgrade (Must Purchase)",
                     "(Unlock all features above, including unlimited chunk size, custom speed, and saves)");


        setListAdapter(new PurchasesAdapter(getActivity(), purchasesList, android.R.layout.two_line_list_item,
                                            new String[]{"purchaseDescription", "purchaseExpanded"},
                                            new int[]{android.R.id.text1, android.R.id.text2}));
        adjustLayoutForPurchases();
    }

    void adjustLayoutForPurchases() {
        if (getListAdapter() != null && getListAdapter().isEmpty()) {
            getListView().setVisibility(View.GONE);
            tvNoPurchases.setVisibility(View.VISIBLE);
        }
    }

    private void maybeAddItem(String sku, List<HashMap<String, String>> purchasesList, String description, String expanded) {
        if (!activity.isPurchased(MainActivity.SKU_PREMIUM)) {
            if (!activity.isPurchased(sku)) {
                HashMap<String, String> purchaseItem = new HashMap<String, String>();
                purchaseItem.put("purchaseDescription", description);
                purchaseItem.put("purchaseExpanded", expanded);
                purchaseItem.put("purchaseId", sku);
                purchasesList.add(purchaseItem);
            }
        }
    }

    void maybeRemoveItem(String sku) {
        if (sku.equals(MainActivity.SKU_DISPLAY) || sku.equals(MainActivity.SKU_ADS)) {
            ((PurchasesAdapter) getListAdapter()).removeItem(sku);
            ((PurchasesAdapter) getListAdapter()).notifyDataSetInvalidated();
        } else if (sku.equals(MainActivity.SKU_PREMIUM)) {
            ((PurchasesAdapter) getListAdapter()).removeAll();
        }
    }


    private String limitOrPremium(int limit) {
        if (!prefs.isPremium().get()) {
            return String.valueOf(limit);
        } else {
            return "Unlimited";
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        activity.purchaseIfReady(((PurchasesAdapter) getListAdapter()).getSku(position));
        l.invalidateViews();
    }

    @Click(R.id.bRestorePurchases)
    void restorePurchases() {
        if (isAdded()) {
            MainActivity activity = (MainActivity) getActivity();
            activity.restorePurchases();
            Toast.makeText(activity, "Purchases restored!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Simple Adapter implementation for ListView, giving each purchase item a product sku string
     */
    class PurchasesAdapter extends SimpleAdapter {
        List<? extends Map<String, ?>> purchases;

        public PurchasesAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            purchases = data;
        }

        public String getSku(int position) {
            return String.valueOf(purchases.get(position).get("purchaseId"));
        }

        public void removeItem(String sku) {
            for (Map<String, ?> item : purchases) {
                if (item.get("productId").equals(sku)) {
                    purchases.remove(item);
                }
            }
        }

        public void removeAll() {
            purchases.removeAll(purchases);
        }
    }
}

package com.shockwave.readerproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Confirmation Dialog Fragment asking if user really wants to reset
 * Created by Nolan Schock, 2014
 */
@EFragment
public class ConfirmationDialogFragment extends DialogFragment {
    private ConfirmationDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        try {
            mListener = (ConfirmationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment().toString() + " must implement " +
                                         "ConfirmationDialogListener");
        }
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Reset?")
                .setMessage("Are you sure you want to do that?\n" +
                            "This action cannot be undone.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onConfirmationPositiveClick(ConfirmationDialogFragment.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    /**
     * Listener implemented in main activity to call reset on applicable fragments
     */
    public interface ConfirmationDialogListener {
        public void onConfirmationPositiveClick(DialogFragment fragment);

    }
}

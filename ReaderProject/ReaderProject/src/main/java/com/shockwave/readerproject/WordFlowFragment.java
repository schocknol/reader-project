package com.shockwave.readerproject;

import android.os.Handler;
import android.os.SystemClock;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

/**
 * Word flow fragment displaying one chunk of text from a passage at a time, and switching between
 * texts at the set pace.
 * Designed to help user increase reading speed.
 */
@EFragment(R.layout.fragment_word_flow)
public class WordFlowFragment extends ReaderFragment {
    private String[] splitText;
    private Handler updateTextHandler;
    private Runnable updateText;

    @InstanceState
    int currentPos = 0;

    @InstanceState
    SavedTextHolder.SavedText currentText;

    @ViewById
    FrameLayout flowLayout;

    @ViewById
    TextView tvFlowReader;

    @AfterViews
    void setupFlow() {
        tvFlowReader.setTextSize(prefs.fontSizeFlow().get());

        if (currentText == null) {
            currentText = dbHandler.getCurrentText(CURRENT_TEXT_KEY_FLOW);
        }
        splitText = currentText.content.split("\\s");

        //Set current position back a chunk
        //Cheap hack to prevent skipping forward (bad idea, shouldn't do this)
        if (currentPos > 0) {
            currentPos -= prefs.wpc().get();
            if (currentPos < 0) {
                currentPos = 0;
            }
        }

        updateTextHandler = new Handler();
        updateText = new Runnable() {
            @Override
            public void run() {
                StringBuilder sb = new StringBuilder();
                if (currentPos >= splitText.length - prefs.wpc().get()) {
                    //reached end of text, stop and reset to beginning
                    sb = new StringBuilder();
                    createChunk(sb, splitText.length - currentPos);
                    currentPos = 0;
                    isRunning = false;
                    stopReading(updateTextHandler, updateText);
                    return;
                }
                //if the amount of words left < wpc, adjust amount of words shown accordingly
                createChunk(sb, prefs.wpc().get());
                currentPos += prefs.wpc().get();
                updateTextHandler.postDelayed(updateText, 1000 / getWordsPerSec(prefs.wpm().get()));
            }
        };

        createChunk(new StringBuilder(), prefs.wpc().get());
        if (isRunning) {
            startReading(updateTextHandler, updateText, prefs.wpm().get());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isRunning) {
            stopReading(updateTextHandler, updateText);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void startReading(Handler handler, Runnable runnable, int wordsPerMin) {
        super.startReading(handler, runnable, wordsPerMin);

        if (!isAdded()) {
            return;
        }

        flowLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable.customAttrs)
                                              .getDrawable(R.styleable.customAttrs_ic_pause));
        hidePauseButton(flowLayout);

    }

    @Override
    protected void stopReading(Handler handler, Runnable runnable) {
        super.stopReading(handler, runnable);

        if (!isAdded()) {
            return;
        }

        if (!isRunning) {
            flowLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable
                                                                                  .customAttrs)
                                                  .getDrawable(R.styleable.customAttrs_ic_play));
        } else {
            flowLayout.setForeground(getActivity().obtainStyledAttributes(R.styleable
                                                                                  .customAttrs)
                                                  .getDrawable(R.styleable.customAttrs_ic_pause));
            hidePauseButton(flowLayout);
        }
    }

    @Override
    public void resetReader() {
        currentPos = 0;
        setWordsPerMin(prefs.wpm().get());
        setWordsPerChunk(1);
    }

    @Click(R.id.flowLayout)
    void flow() {
        if (isRunning) {
            if (!pauseClick) {
                //Briefly show UI to allow user to do something
                ((MainActivity) getActivity()).showSystemUI();
                hideUIOnDelay((MainActivity) getActivity());
                pauseClick = true;
            } else {
                stopReading(updateTextHandler, updateText);
                pauseClick = false;
            }
        } else {
            startReading(updateTextHandler, updateText, prefs.wpm().get());
        }
    }

    @Click(R.id.tvFlowReader)
    void flowIntercepted() {
        flow();
    }

    /**
     * Creates the chunk of words to display on screen
     * Loops up to wordsPerChunk and appends each word to create the chunk
     *
     * @param sb            StringBuilder to append words to
     * @param wordsPerChunk Integer number of words to display
     */
    private void createChunk(StringBuilder sb, int wordsPerChunk) {
        for (int i = 0; i < wordsPerChunk; i++) {
            if (currentPos + i < splitText.length) {
                sb.append(splitText[currentPos + i]);
            } else {
                sb.append(splitText[splitText.length - 1]);
            }
            sb.append(" ");
        }
        tvFlowReader.setText(sb);
    }

    @Override
    protected void setWordsPerChunk(int wpc) {
        super.setWordsPerChunk(wpc);
        createChunk(new StringBuilder(), prefs.wpc().get());
    }

    @Override
    public void onDialogNumberSet(int reference, int number, double decimal, boolean isNegative,
                                  double fullNumber) {
        switch (reference) {
            case WPC_REFERENCE:
                setWordsPerChunk(number);
                break;
            case SPEED_REFERENCE:
                setWordsPerMin(number);
                break;
        }
    }

    @Override
    protected void setTextFromString(String text) {
        currentText = new SavedTextHolder.SavedText("Unknown", "Unknown", text, SystemClock
                .elapsedRealtime());
        dbHandler.setCurrentTextFlow(currentText);
        splitText = text.split("\\s");
        currentPos = 0;
        createChunk(new StringBuilder(), prefs.wpc().get());
    }

    @Override
    protected void setTextFromSavedText(SavedTextHolder.SavedText text) {
        currentText = text;
        dbHandler.setCurrentTextFlow(currentText);
        splitText = text.content.split("\\s");
        currentPos = 0;
        createChunk(new StringBuilder(), prefs.wpc().get());
    }

    @Override
    protected SavedTextHolder.SavedText getText() {
        return currentText;
    }

    @Override
    void changeWordsPerChunk() {
        stopReading(updateTextHandler, updateText);
        super.changeWordsPerChunk();
    }

    @Override
    void selectText() {
        stopReading(updateTextHandler, updateText);
        super.selectText();
    }

}
